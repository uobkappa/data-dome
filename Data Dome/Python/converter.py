# Converter Library for XLS -> SQL table
from excel import *
from os import listdir
from re import match, sub as replace
import sqlite3 as sql

def load_database(filename):
    return sql.connect(filename)

def get_column_names(sheet):
    names = (name.title() for name in sheet.row(0))
    names = (replace(r'[_ ]', '', name) for name in names)
    names = (replace(r'[%^(){}+-/\[\]]', '_', name) for name in names)
    return [replace(r'^(\d)', r'_\g<0>', name) for name in names]

def get_table_name(filename):
    name = filename.split('.')[0].split('\\')[-1].title()
    name = replace(r'[_ ]', '', name)
    name = replace(r'[%^(){}+-/\[\]]', '_', name)
    return replace(r'^(\d)', r'_\g<0>', name)

def process_excel_to_sql(filename, database, close=False):
    try:
        with database:
            sheet = Excel(filename).get_sheet(0)
            column_names = get_column_names(sheet)
            table_name = get_table_name(filename)
            database.execute("DROP TABLE IF EXISTS %s" % table_name)
            database.execute("CREATE TABLE %s (%s)" % (table_name, ", ".join(column_names)))
            print "Adding table: %s" % table_name
            for i in xrange(1, sheet.number_rows()):
                row = sheet.row(i)
                database.execute("INSERT INTO %s VALUES (%s)" % (table_name, ", ".join(('?' for _ in xrange(len(row))))), row)
    except sql.IntegrityError as e: print e
    except sql.OperationalError as e: print e
    finally:
        if close:
            database.close()

def process_directory(directory, database, close=False):
    for spreadsheet in (spreadsheet.split('.')[0] for spreadsheet in listdir(directory) if match(r".+\.xls", spreadsheet)):
        path = directory + '\\' + spreadsheet + ".xls"
        process_excel_to_sql(path, database)
    if close:
        database.close()

if __name__ == "__main__":
    # Example using a directory in this path called data, writing to data.sql
    #db = load_database("data.sql")
    #process_directory("data", db, True)
    pass

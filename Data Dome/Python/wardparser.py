import sqlite3 as sql

lines = []
with file("wards.csv", 'r') as f:
    lines = f.readlines()

name_map = {}
data = []
for line in lines[1:]:
    name, x, y = line.strip().split(',')
    if not name in name_map: name_map[name] = len(name_map)
    data.append((name_map[name], name, x, y))

conn = sql.connect('..\Assets\data.sql')
try:
    conn.execute('DROP TABLE Wards')
    conn.execute('CREATE TABLE Wards (wardId int, wardName text, x real, y real)')
    for d in data:
        conn.execute('INSERT INTO Wards VALUES (%s, \'%s\', %s, %s)' % (d))
        print d
    conn.execute('DROP TABLE WardMidPoints')
    conn.execute('CREATE TABLE WardMidPoints (wardId int, wardName text, x real, y real)')
    for i in xrange(133):
        data_i = filter(lambda d : d[0] is i, data)
        data_i_xs = map(lambda d : float(d[2]), data_i)
        data_i_ys = map(lambda d : float(d[3]), data_i)
        x = (max(data_i_xs) + min(data_i_xs))/2.0
        y = (max(data_i_ys) + min(data_i_ys))/2.0
        print (i, data_i[0][1], x, y)
        conn.execute('INSERT INTO WardMidPoints VALUES (%s, \'%s\', %s, %s)' % (i, data_i[0][1], x, y))
        
except Exception:
    print "exception has occured"
finally:
    conn.commit()
    conn.close()


﻿using UnityEngine;
using System.Collections;

public class DontDestroy : MonoBehaviour {

	private static DontDestroy instance = null;
	public static DontDestroy Instance {
		get { return instance; }
	}

	void Awake() {
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Alpha2)) {
			Application.LoadLevel("Energy Use");
		}
		else if (Input.GetKey (KeyCode.Alpha3)) {
			Application.LoadLevel("solarcommulative");
		}
		else if (Input.GetKey (KeyCode.Alpha1)) {
			Application.LoadLevel("Untitled");
		}
	}
}

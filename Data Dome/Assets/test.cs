﻿using UnityEngine;
using System.Collections;

public class test : MonoBehaviour {
    [SerializeField] public Texture texDef;
    [SerializeField] public Texture tex0;
    [SerializeField] public Texture tex1;
    [SerializeField] public Texture tex2;

    private bool up = false;
    bool pressed = false;
    float scale = 0.01f;
    int blendTimer = 30;

    float currentTex = 0.0f;
    int currentLast = 0;

    // Use this for initialization
    void Start() {
        currentTex = 0.0f;
    }

    // Update is called once per frame
    void Update() {
        if (!pressed && Input.GetKeyDown(KeyCode.Space)) {
            up = !up;
            pressed = true;
        } else {
            pressed = false;
        }

        if (up) {
            if (scale < 2.0f) {
                scale += (2.0f - scale) * 0.075f;
            } else {
                scale = 2;
            }
        } else {
            if (scale > 0.0f) {
                scale += (0.0f - scale) * 0.075f;
            } else {
                scale = 0.0f;
            }

        }

        this.transform.localScale = new Vector3(1.0f, 1.0f, scale);



        // Perform blending
        if (blendTimer <= 0) {
            currentTex += 0.01f;
        }else {
            blendTimer--;
        }
        Material m = this.GetComponent<MeshRenderer>().material;

        int current = (int)Mathf.Floor(currentTex);
        int next    = (int)Mathf.Ceil(currentTex);


        // Shift textures along 
        m.SetFloat("_merge", currentTex % 1.0f);
        if( current != currentLast  && current < 4) {
            currentLast = current;
            Texture tt, tt2;
            tt  = getTexFromID(current);
            tt2 = getTexFromID(current+1);
            m.SetTexture("_MainTex", tt);
            m.SetTexture("_MainTex2", tt2);
            blendTimer = 100;
        }
        if (currentTex > 4) currentTex = 0;

    }

    public Texture getTexFromID(int id) {
        switch (id) {
            case 0: return texDef; break;
            case 1: return tex0; break;
            case 2: return tex1; break;
            case 3: return tex2; break;
            case 4: return texDef; break;
        }
        return null;
    }
}
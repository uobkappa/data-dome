using UnityEngine;
using System.Collections;
using SQLiteInterface;
using System;
public class County : ScriptableObject 
{
	public Transform ctext;
	public float x, y, offsetX, offsetY, offsetZ;
	public string title;
	public GameObject text;
	private Vector3 position;

	public County() {}

	public void init(string title, Vector3 position, GameObject parent)
	{
		this.title = title;
		//this.position = new Vector3(position.x/10, position.z/10, 0.0f);
		this.position = new Vector3 (position.x, position.y, position.z + 10.0f);//new Vector3(position.z, position.x, 0.0f);


		addToGame (parent);
	}
	
	private void addToGame(GameObject parent)
	{
		//Canvas canvasComponent = GameObject.Find("Canvas").GetComponent<Canvas>();
		text = Instantiate(Resources.Load("ctext", typeof(GameObject))) as GameObject;
		text.transform.position = position;
		text.transform.localScale = new Vector3 (-20.0f, 20.0f, 20.0f);
		//text.transform.SetParent(parent.gameObject.transform, false);
		TextMesh theText = text.transform.GetComponent<TextMesh>();
		theText.text = title;
	}


	//UNUSED
	public void step()
	{
		//offsetX = 0;
		//offsetY = 0;
		//offsetZ = Mathf.Sin(delta/20)*50;
		//text.transform.position = new Vector3(x+offsetX, offsetZ, y+offsetY);
		text.transform.LookAt(Camera.main.transform.position, Vector3.up);
	}
}
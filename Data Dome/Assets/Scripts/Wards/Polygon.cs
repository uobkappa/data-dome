﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Polygon : MonoBehaviour 
{
	// Use this for initialization
    //public Vector3[] newVertices = new Vector3[] {new Vector3(0,100,-15), new Vector3(0,0,-15), new Vector3(100,1,-15)};
    public List<Vector3> newVertices;
    public List<Vector2> newUV;
    //public Vector2[] newUV = new Vector2[] {new Vector2(0,100), new Vector2(0,0), new Vector2(100,1)};
    public int[] newTriangles = new int[] {0,1,2,1,2,3};
    void Start () 
	{
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        foreach (Transform child in transform) 
		{
            Debug.Log(child.position);
            newVertices.Add(child.position);
            newUV.Add(new Vector2(child.position.x, child.position.y));
        }
        mesh.vertices = newVertices.ToArray();
        mesh.uv = newUV.ToArray();
        mesh.triangles = newTriangles;
	}
}

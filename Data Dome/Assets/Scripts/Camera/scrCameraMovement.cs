using UnityEngine;
using System.Collections;



public class scrCameraMovement : MonoBehaviour 
{
	private const float SPEED_OF_PAN = 2000.0f;
	private const float ZOOM_SPEED = 7500.0f;
	private const int CLAMP_MAX = 10000;
	private const int CLAMP_MIN = 5000;

	private Vector2 top_left = new Vector2(0.0f, -3000.0f);
	private Vector2 bottom_right = new Vector2(1000.0f, -3000.0f);
	private Vector3 source;
	private Vector3 etarget, target;
	private Vector3 lookat = new Vector3(500, 100, 500);

	private bool animation = true;
	private bool target_set = false;

	private float progress = 0.0f;
	private float ticks = 0.0f;

	// Update is called once per frame
	void Update() 
	{
		ticks += Time.deltaTime;
		if (!animation) {
			float zoom_mod = ((transform.position.y - CLAMP_MIN) / (CLAMP_MAX - CLAMP_MIN)) + 1;
			if (Input.GetKey (KeyBindings.CAMERA_LEFT)) {
				transform.position += Vector3.left * SPEED_OF_PAN * Time.deltaTime * zoom_mod;
			}
			if (Input.GetKey (KeyBindings.CAMERA_RIGHT)) {
				transform.position += Vector3.right * SPEED_OF_PAN * Time.deltaTime * zoom_mod;
			}
			if (Input.GetKey (KeyBindings.CAMERA_DOWN)) {
				transform.position += Vector3.back * SPEED_OF_PAN * Time.deltaTime * zoom_mod;
			}
			if (Input.GetKey (KeyBindings.CAMERA_UP)) {
				transform.position += Vector3.forward * SPEED_OF_PAN * Time.deltaTime * zoom_mod;
			}
			if (Input.GetAxis ("Mouse ScrollWheel") > 0) {
				transform.position += Vector3.down * ZOOM_SPEED * Time.deltaTime;
			}
			if (Input.GetAxis ("Mouse ScrollWheel") < 0) {
				transform.position += Vector3.up * ZOOM_SPEED * Time.deltaTime;
			}
			// Clamp position
			Vector3 clamp = transform.position;
			clamp.y = Mathf.Clamp(transform.position.y, CLAMP_MIN, CLAMP_MAX);
			transform.position = clamp;
		} else {
			//Random Camera Movement
			if(!target_set){
				target_set = true;
				source = new Vector3(transform.position.x, transform.position.y, transform.position.z);
				//Debug.Log(source);
				//target = new Vector3(Random.Range(top_left.x, bottom_right.x), Random.Range(CLAMP_MIN, CLAMP_MAX), Random.Range(bottom_right.y, top_left.y));
				etarget = StaticWard.WardNames.getRandom();
				progress = 0.0f;
			}

			target = tween(target, etarget, 0.1f);
			Vector3 actual_target = new Vector3(target.x+Mathf.Sin(ticks/5)*5000, 10000, target.z+Mathf.Cos(ticks/5)*5000);
			//Vector3 actual_target = new Vector3(5, 5, 5);
			transform.position = tween(transform.position, actual_target, 0.3f);

			//transform.position = new Vector3(intermediate(source.x, target.x, progress), intermediate(source.y, target.y, progress), intermediate(source.z, target.z, progress));
			transform.LookAt(target);

			if(progress > 3){
				target_set = false;
				progress = 0.0f;
			}else{
				progress += Time.deltaTime;
			}
		}
	}

	private Vector3 tween(Vector3 current, Vector3 target, float speed){
		//return target;
		float dx = (target.x - current.x) * Time.deltaTime;
		float dy = (target.y - current.y) * Time.deltaTime;
		float dz = (target.z - current.z) * Time.deltaTime;

		float ox = dx * speed;
		float oy = dy * speed;
		float oz = dz * speed;

		return new Vector3 (current.x+ox, current.y+oy, current.z+oz);
	}

	private float intermediate(float x1, float x2, float w){
		return (x2 * w) + (x1 * (1.0f - w));
	}
}
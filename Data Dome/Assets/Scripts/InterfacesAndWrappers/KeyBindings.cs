using UnityEngine;
using System.Collections;

/// <summary>
/// This is the class that holds all our keybindings. In the future this should be moved to a metadata file for user customisation.
/// </summary>
public class KeyBindings 
{
	/// <summary>
	/// Moves camera to the left.
	/// </summary>
	public const KeyCode CAMERA_LEFT = KeyCode.LeftArrow;
	/// <summary>
	/// Moves camera to the right.
	/// </summary>
	public const KeyCode CAMERA_RIGHT = KeyCode.RightArrow;
	/// <summary>
	/// Moves camera to up.
	/// </summary>
	public const KeyCode CAMERA_UP = KeyCode.UpArrow;
	/// <summary>
	/// Moves camera to down.
	/// </summary>
	public const KeyCode CAMERA_DOWN = KeyCode.DownArrow;
	/// <summary>
	/// Moves timeline along by a unit.
	/// </summary>
	public const KeyCode TIMELINE_INCREMENT = KeyCode.Equals;
	/// <summary>
	/// Moves timeline back by a unit.
	/// </summary>
	public const KeyCode TIMELINE_DECREMENT = KeyCode.Minus;
}

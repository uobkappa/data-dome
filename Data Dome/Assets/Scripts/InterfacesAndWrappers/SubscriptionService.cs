using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Delegate for any action that should be performed on some event
/// </summary>
public delegate void NotifyAction<T>(T notifier);

/// <summary>
/// Handles the observer pattern subscription model for some object, acting as a "contractor".
/// It should be used when you want objects to respond to some event that is associated with
/// another class with a specific functionality (like updating some internal state). The
/// observer pattern model used is the push model.
/// </summary>
public class SubscriptionService<T, E>
{
	private Dictionary<E, List<NotifyAction<T>>> subscriptions = new Dictionary<E, List<NotifyAction<T>>>();
	private T owner;

	/// <summary>
	/// Initializes a new instance of the <see cref="SubscriptionService"/> class.
	/// </summary>
	/// <param name="owner">This is the object that the events are being handled for.
	/// When an event is triggered, this object is passed into the NotifyAction</param>
	public SubscriptionService(T owner)
	{
		this.owner = owner;
		foreach (E eventType in Enum.GetValues(typeof(E)))
		{
			subscriptions[eventType] = new List<NotifyAction<T>>();
		}
	}

	/// <summary>
	/// Adds the specifified action (which may be bound to an object) to a 
	/// specific event. When that event is triggered, the action will be executed.
	/// </summary>
	/// <param name="action">Action to perform on event</param>
	/// <param name="eventType">Event to bind to</param>
	public void Subscribe(NotifyAction<T> action, E eventType)
	{
		subscriptions[eventType].Add(action);
	}

	/// <summary>
	/// Notify all subscribers that an event has happened, this will call all
    /// subscribed methods in the order they were subscribed to.
	/// </summary>
	/// <param name="eventType">Event that has occured</param>
	public void Notify(E eventType)
	{
		foreach (var subscriber in subscriptions[eventType])
		{
			subscriber.Invoke(owner);
		}
	}
}



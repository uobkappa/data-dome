﻿using UnityEngine;
using System.Collections;
using System;

public class Scales
{
    public static Color thermalScale(float valueToScale, float minValue, float maxValue)
	{
		float red = 1.0f;
		float green = 1.0f;
		float blue = 1.0f;
        
		valueToScale = Mathf.Clamp(valueToScale, minValue, maxValue);
        
        float range = maxValue - minValue;
        
        if (valueToScale < (minValue + 0.25f * range))
		{
            red = 0.0f;
            green = 4.0f * (valueToScale - minValue) / range;
        } 
		else if (valueToScale < (minValue + 0.5f * range)) 
		{
            red = 0.0f;
            blue += 4.0f * (minValue + 0.25f * range - valueToScale) / range;
        } 
		else if (valueToScale < (minValue + 0.75 * range))
		{
            red = 4.0f * (valueToScale - minValue - 0.5f * range) / range;
            blue = 0.0f;
        } 
		else 
		{
            green += 4.0f * (minValue + 0.75f * range - valueToScale) / range;
            blue = 0.0f;
        }
		return new Color ((red+1)/2, (green+1)/2, (blue+1)/2, 1.0f);
    }

    public static float basicNumerical(float valueToScale, float minValue, float maxValue)
    {
		return Mathf.Clamp01(((valueToScale - minValue) / (maxValue - minValue)));
    }
}

﻿using UnityEngine;
using Mono.Data.Sqlite;
using System;
using System.Data;
using System.Collections.Generic;

namespace SQLiteInterface
{
    public class SQLiteDatabase
    {
		private static Dictionary<string, SQLiteDatabase> staticDatabases = new Dictionary<string, SQLiteDatabase>();

        private string url;
		private string shortenedUrl;
        private SqliteConnection connection;
        private SqliteTransaction transaction;

        public SQLiteDatabase(string databaseName)
        {   
            url = "URI=file:" + Application.dataPath + "/" + databaseName;
			shortenedUrl = databaseName;
        }

        ~SQLiteDatabase()
        {
			CloseConnection();
        }

		/// <summary>
		/// Either returns the currently open statically loaded database with given name or opens it, also opening the connection.
		/// We should use this for when we need a database kept open all the time (like data.sql). Use the plain constructor for
	    /// temporary databases that should be closed. Databases loaded here should NOT be closed, on pain of death.
		/// </summary>
		/// <param name="name">Name of database</param>
		/// <returns>The database with the given name, with an open connection</returns>
		public static SQLiteDatabase Get(string name)
		{
			name += name.EndsWith(".sql") ? "" : ".sql";
			if (!staticDatabases.ContainsKey(name))
			{
				staticDatabases[name] = new SQLiteDatabase(name);
				staticDatabases[name].OpenConnection();
			}
			return staticDatabases[name];
		}

        /// <summary>
        /// Open connection to database
        /// </summary>
        /// <remarks>Creates a new connection and calls the Open method on it. Make sure to call CloseConnection</remarks>
        /// <returns>true if connection was successful, false otherwise</returns>
        public bool OpenConnection()
        {
            try
            {
                connection = new SqliteConnection(url);
                connection.Open();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns an SQLiteQuery object bound to this database and string query
        /// </summary>
        /// <param name="query">Query to be executed</param>
        /// <returns>SQLiteQuery object that represents the operation</returns>
        public SQLiteQuery Query(string query)
        {
            SQLiteQuery newQuery = new SQLiteQuery(connection.CreateCommand());
            newQuery.SetQuery(query);
            return newQuery;
        }

        /// <summary>
        /// Starts a batch operation. It is only used for mutators on the database, no results can be collected!!!
        /// </summary>
        public void BeginBatch()
        {
            transaction = connection.BeginTransaction();
        }

        /// <summary>
        /// Adds another query to the currently building batch. Make sure that BeginBatch() has been called first!!!
        /// </summary>
        /// <param name="query">Query.</param>
        public void AddToBatch(string query)
        {
            SqliteCommand com = connection.CreateCommand();
            com.CommandText = query;
            com.ExecuteNonQuery();
        }

        /// <summary>
        /// Runs the currently compiled batch operations, which is every query added to the batch since the call to BeginBatch()
        /// </summary>
        public void ExecuteBatch()
        {
            transaction.Commit();
        }

        /// <summary>
        /// Tests whether the connection is open
        /// </summary>
        /// <returns>Returns true if the connection is currently open, else false</returns>
        public bool IsOpen()
        {
			if (connection == null) return false;
            return connection.State == ConnectionState.Open;
        }

        /// <summary>
        /// Closes any OPEN connection to the database, also removes it from the static databases just in case.
        /// </summary>
        public void CloseConnection()
        {
            if (connection.State != ConnectionState.Closed) connection.Close();
			if (staticDatabases.ContainsKey(this.shortenedUrl))
			{
				staticDatabases.Remove(this.shortenedUrl);
			}
        }
    }

    public class SQLiteQuery
    {
        private SqliteCommand command;
        private SqliteDataReader reader;
		private List<object[]> resultsArray;
		private List<List<object>> transposeResultsArray;

        public SQLiteQuery(SqliteCommand command)
        {
            this.command = command;
			this.reader = null;
			this.resultsArray = null;
			this.transposeResultsArray = null;
        }

        public void SetQuery(string query)
        {
            command.CommandText = query;
        }

        /// <summary>
        /// Runs the query on the database
        /// </summary>
        public void Execute()
        {
            reader = command.ExecuteReader();
        }

        /// <summary>
        /// Produces an iterator over the results of the query (use foreach). Returns an object array, which is the results from the queried columns
        /// </summary>
        /// <remarks>for SELECT Year, RowID, Name FROM SomeTable will return object [3], [0] is Year, [1] is RowID etc</remarks>
        /// <returns>Iterator of results from query</returns>
        public System.Collections.Generic.IEnumerable<object[]> Results()
        {
			if (reader == null) throw new SQLiteQueryException("Attempting to collect results without having executed the query");
            while (reader.Read())
            {
                object[] result = new object[reader.FieldCount];
				for (int i = 0; i < reader.FieldCount; i++) result[i] = reader[i];
                yield return result;
            }
            yield break;
        }

        /// <summary>
        /// Wrapper for creation of List<object []> from the Results() method. For use if you need the results more than once or want it stored in a List
        /// for direct access to result rows.
        /// </summary>
        /// <returns>The list of results from the query</returns>
        public List<object[]> ResultsArray()    
        {
			if (resultsArray != null) return resultsArray;
            resultsArray = new List<object []>();
            foreach (object [] result in Results())
            {
				resultsArray.Add(result);
            }
			return resultsArray;
        }

		/// <summary>
		/// Returns the results array such that instead of indexing by row then column, you index by column then row.
		/// </summary>
		/// <returns>The transposed results array</returns>
		public List<List<object>> TransposeResultsArray()
		{
			if (transposeResultsArray != null) return transposeResultsArray;
			transposeResultsArray = new List<List<object>>();
			foreach (object[] result in ResultsArray())
			{
				for (int i = 0; i < result.Length; i++)
				{
					if (transposeResultsArray.Count == i) transposeResultsArray.Add(new List<object>());
					transposeResultsArray[i].Add(result[i]);
				}
			}
			return transposeResultsArray;
		}
    }

	public class SQLiteQueryException : Exception
	{
		public SQLiteQueryException() : base() {}
		public SQLiteQueryException(string message) : base(message) {}
		public SQLiteQueryException(string message, Exception inner) : base(message, inner) {}
	}
}

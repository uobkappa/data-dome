using System;
using SQLiteInterface;

public class ConstantHelpers
{
    // CONSTANTS
	/// <summary>
	/// Maximum energy consumption for the entire city.
	/// </summary>
    public static readonly double BRISTOL_MAX_ENERGY_CONSUMPTION;
	/// <summary>
	/// Minimum year that the timeline can be scrolled to, i.e the lower bound of our data.
	/// </summary>
	public const int MIN_YEAR = 2007;
	/// <summary>
	/// Maximum year that the timeline can be scrolled to, i.e the upper bound of our data.
	/// </summary>
	public const int MAX_YEAR = 2010;
	/// <summary>
	/// The positional offset in x of the timeline, in pixels
	/// </summary>
	public const int TIMELINE_OFFSET_X = 0;
	/// <summary>
	/// The positional offset in y of the timeline, in pixels
	/// </summary>
	public const int TIMELINE_OFFSET_Y = 0;
	/// <summary>
	/// The amount of years that are displayed either side of the current year on the timeline.
	/// </summary>
	public const int TIMELINE_YEARS_EITHER_SIDE = 2;
	/// <summary>
	/// SQL Query that collects the maximum energy consumption for BRISTOL_MAX_ENERGY_CONSUMPTION.
	/// </summary>
    private const string QUERY_MAX_CONSUMPTION = "SELECT max(EnergyConsumption) FROM EnergyConsumptionByWard_AnnualTotal_Normalised"; 
	/// <summary>
	/// Adjusts the speed at which visualisation effects transition between different values. Lower the value the slower the transition.
	/// </summary>
	public const float ANIMATION_SMOOTH_FACTOR = 0.1f;
    
	// HELPERS
	static ConstantHelpers()
	{
        BRISTOL_MAX_ENERGY_CONSUMPTION = ComputeBristolMaxEnergyConsumption();
    }

    private static double ComputeBristolMaxEnergyConsumption() 
    {
		SQLiteQuery query = SQLiteDatabase.Get("data").Query(QUERY_MAX_CONSUMPTION);
        query.Execute();
        return (double) query.ResultsArray()[0][0];
    }
}


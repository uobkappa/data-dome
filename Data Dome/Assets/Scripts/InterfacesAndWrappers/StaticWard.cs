﻿using UnityEngine;
using System.Collections;
using System;

namespace StaticWard {
	public class WardNames {
		private static Visualisation[] visualisations = new Visualisation[100];
		private static int count = 0;

		private static System.Random r = new System.Random();

		public static void addVisualisation(Visualisation v){
			visualisations[count++] = v;
		}

		public static Vector3 getRandom(){
			double target = r.NextDouble() * count;
			return visualisations [Mathf.FloorToInt ((float) target)].center;
		}
	}
}
using UnityEngine;
using System.Collections;

public class EnergyScale : Scale 
{
	public override void Transform() 
	{
		transform.GetChild(0).GetComponent<Renderer>().material.color = Scales.thermalScale(percentage, minimum, maximum);
        transform.localScale = new Vector3(1.0f, 0.1f + 0.01f*((float)percentage), 1.0f);
    }
}

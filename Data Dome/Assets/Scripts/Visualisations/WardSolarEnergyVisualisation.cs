﻿using UnityEngine;
using System.Collections;
using SQLiteInterface;
using System;

public class WardSolarEnergyVisualisation : Visualisation 
{
	public float currentSolarEnergy = 0;
	private Projection projection;

	// Queries
	//private const string QUERY_POSTCODE_FROM_WARD = "SELECT PostCode FROM WardToPostCode WHERE WardName={0}";
	private const string QUERY_SOLAR_ENERGY_FOR_YEAR =  "SELECT InstalledCapacity, ApplicationDate, A.PostCode " +
														"FROM SolarInstallationsByYearAndPostcode A " +
														"INNER JOIN (SELECT PostCode FROM WardToPostCode WHERE WardName='{0}') B " +
														"ON A.PostCode = B.PostCode";
	private const string QUERY_SOLAR_MAX_CONSUMPTION = "???";
	private const string QUERY_SOLAR_MIN_CONSUMPTION = "???";
	private const string QUERY_SOLAR_ENERGY_FOR_ALL_YEARS = "???";

	//private const SQLiteQuery allQuery;
	
	// Memoization
	private float lowerYear = 0;
	private float upperYear = 0;
	private float lowerYearEnergy = 0;
	private float upperYearEnergy = 0;
	private float[] cumulativeCapacity = new float[6];

	// Use this for initialization
	public override void Start()
	{
		//minQuery = SQLiteDatabase.Get("data").Query(QUERY_SOLAR_MIN_CONSUMPTION);
		//maxQuery = SQLiteDatabase.Get("data").Query(QUERY_SOLAR_MAX_CONSUMPTION);
		base.Start();
		center = transform.GetChild(0).GetComponent<Renderer>().bounds.center;
		text = ScriptableObject.CreateInstance("County") as County;
		text.init (wardname, center, gameObject);

		//Initialize projection
		SQLiteQuery postcodes = SQLiteDatabase.Get("data").Query(String.Format(QUERY_SOLAR_ENERGY_FOR_YEAR, wardname));
		postcodes.Execute();

		//Debug.Log (postcodes.ResultsArray ());
		//Debug.Log (postcodes.TransposeResultsArray());
		//Debug.Log (postcodes.ResultsArray ());

		//ArrayList<object[]> resultsArray = postcodes.ResultsArray();
		foreach(object[] result in postcodes.ResultsArray()) {
			
			// Calculate all cummulative terms for every year
			int yearV = Convert.ToInt32(result[1]);
			for( int yy = 2015; yy >= yearV; yy -- ){
				cumulativeCapacity[(yy-2010)] += Convert.ToSingle(result[0]);
			}
		}

		// Output values:
		/*allQuery = SQLiteDatabase.Get("data").Query(String.Format(QUERY_SOLAR_ENERGY_FOR_ALL_YEARS, wardname));
		allQuery.Execute();*/
		System.Collections.Generic.List<System.Collections.Generic.List<object>> ptrans = postcodes.TransposeResultsArray ();
		if (ptrans.Count > 1) {
			float[] xs = Array.ConvertAll (ptrans [1].ToArray (), item => Convert.ToSingle (item));
			float[] ys = Array.ConvertAll (ptrans [0].ToArray (), item => Convert.ToSingle (item));
			projection = new Projection (xs, ys);
		} else {
			projection = new Projection();
		}

		//Debug.Log (ptrans.Count);
		//float[] xs = Array.ConvertAll(postcodes.TransposeResultsArray()[0].ToArray(), item => (float) item);
		//float[] ys = Array.ConvertAll(allQuery.TransposeResultsArray()[1].ToArray(), item => (float) (Int64) item);
		//projection = new Projection(xs, ys);
		

		UpdateVisualisationState(year, Mathf.RoundToInt(year));
	
	}
	
	public override void UpdateVisualisationState(float currentYear, int maxYear)
	{
		if (currentYear > 2015) {
			Debug.Log(currentYear);
			currentSolarEnergy = projection.InterpolateYFromX(currentYear);
			Debug.Log(currentSolarEnergy);
		}else{
			if (Mathf.FloorToInt(currentYear) != lowerYear) 
			{
				lowerYear = Mathf.FloorToInt (currentYear);
				lowerYearEnergy = cumulativeCapacity [(int)Mathf.Clamp((float)lowerYear - 2010, 0, 5)];
			}
			if (Mathf.CeilToInt (currentYear) != upperYear) 
			{
				upperYear = Mathf.CeilToInt (currentYear);
				upperYearEnergy = cumulativeCapacity [(int)Mathf.Clamp((float)upperYear - 2010, 0, 5)];
			}
			float weight = currentYear % 1;
			currentSolarEnergy = (weight * upperYearEnergy) + ((1 - weight) * lowerYearEnergy);
		}
		//currentSolarEnergy = cumulativeCapacity [(int)Mathf.Clamp((float)currentYear - 2010, 0, 5)];
		UpdateVisualisation();
	}

	protected override float GetMinValue()
	{
		return 0.0f;
	}

	protected override float GetMaxValue()
	{
		return 1345.77f;
	}
	
	public override void UpdateVisualisation()
	{
		float bristolMax = GetMaxValue();
		float bristolMin = GetMinValue();
		transform.GetChild(0).GetComponent<Renderer>().material.color = Scales.thermalScale(currentSolarEnergy, bristolMin, bristolMax);
		transform.localScale = new Vector3(1.0f, 1.0f, 0.1f+Scales.basicNumerical(currentSolarEnergy, bristolMin, bristolMax));
	}
}

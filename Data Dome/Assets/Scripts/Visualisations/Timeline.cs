﻿using UnityEngine;
using System;
using UnityEngine.UI;

// Possible Events the Timeline can trigger
public enum TimelineNotificationEvent { YEAR_CHANGE_UP, YEAR_CHANGE_DOWN, ANIMATION_STEP, UPDATE };

public class Timeline : MonoBehaviour
{
	// UNITY MEMBER VARIABLES
    public Text timeText;
	public Transform TimelineDate;

	// PRIVATE MEMBER VARIABLES
	private TimelineText[] timelineTexts;
	private SubscriptionService<Timeline, TimelineNotificationEvent> subscriptionService;
	private float incrementTime = 0; 
	private float decrementTime = 0;
    private float delayBetweenPresses = 0.5f;
	public int currentYear;
	public int minYear = 2007;
	public int maxYear = 2010;
	private int maxDataProjection = 10; //Number of years AFTER the data set should the timeline go
	private bool decrementPressed = false;
	private bool incrementPressed = false;

	// PRIVATE CONSTANTS
	private const int NONE_PRESSED = 0;
	private const int INCREMENT_PRESSED = 1;
	private const int DECREMENT_PRESSED = -1;
	private const int TOTAL_YEARS_TO_DISPLAY = ConstantHelpers.TIMELINE_YEARS_EITHER_SIDE*2 + 1;
	private const int ADJUSTMENT = 100;
	private readonly int TIMELINE_SPAN = (Screen.width + ADJUSTMENT)/2;
	private readonly int TIMELINE_DIST_FROM_BOTTOM = 50 - Screen.height/2;

	// PRIVATE ANIMATION VARIABLES
	private int originalYear;
	private int targetYear;
	private float animationProgress = 0.0f;

	Timeline()
	{
		subscriptionService = new SubscriptionService<Timeline, TimelineNotificationEvent>(this);
	}

	// UNITY METHODS
	void Start()
    {
		currentYear = minYear;
		targetYear = currentYear;
		originalYear = currentYear;

		int dist = ConstantHelpers.TIMELINE_YEARS_EITHER_SIDE;
		timelineTexts = new TimelineText[TOTAL_YEARS_TO_DISPLAY];
		for (int i = 0; i < TOTAL_YEARS_TO_DISPLAY; i++)
		{
			int year = currentYear - dist + i;
			timelineTexts[i] = ScriptableObject.CreateInstance("TimelineText") as TimelineText;
			timelineTexts[i].init(year.ToString(), TIMELINE_SPAN*((float)i/dist) - ADJUSTMENT/2, TIMELINE_DIST_FROM_BOTTOM);
		}
	}

	void Update() 
	{
		// Auld Mike's nifty arbiter; if both keys are pressed then it returns to 0
		int keyPressed = NONE_PRESSED;
		if (Input.GetKey(KeyBindings.TIMELINE_INCREMENT)) keyPressed++;
		if (Input.GetKey(KeyBindings.TIMELINE_DECREMENT)) keyPressed--;
		switch (keyPressed)
		{
		case INCREMENT_PRESSED:
			if (!incrementPressed) HandleIncrementPressed(); 
			else 
			{
				incrementTime += Time.deltaTime;
				if (incrementTime > delayBetweenPresses) HandleIncrementPressed();
			}
			incrementPressed = true;
			break;
		case DECREMENT_PRESSED:
			if (!decrementPressed) HandleDecrementPressed();
			else
			{
				decrementTime += Time.deltaTime;
				if (decrementTime > delayBetweenPresses) HandleDecrementPressed();
			}
			decrementPressed = true;
			break;
		default:
			incrementPressed = false;
			decrementPressed = false;
			break;
		}
		targetYear = currentYear;
		updateTimelinePositions();
		subscriptionService.Notify(TimelineNotificationEvent.UPDATE);
	}

	// PRIVATE METHODS

	/// <summary>
	/// Updates the timeline position so that it is shifting towards the correct year being central on the screen.
	/// </summary>
	private Timeline updateTimelinePositions()
	{
		//TODO use Camera.current.pixelWidth
		//Possible States: Increasing, Decreasing, Stationary
		if (originalYear != targetYear || animationProgress != 0.0f)
		{
			int direction = Mathf.Clamp(targetYear-originalYear, 1, -1);

			//Compute the animation progress as a fraction
			animationProgress = Mathf.Clamp(animationProgress+Time.deltaTime*3, 0.0f,  1.0f);

			for (int i = 0; i < TOTAL_YEARS_TO_DISPLAY; i++)
			{
				//Computes x_delta: the distance from the start of a year text to the start of the next
				int x_delta = TIMELINE_SPAN/ConstantHelpers.TIMELINE_YEARS_EITHER_SIDE;

				timelineTexts[i].offset(x_delta*animationProgress*direction, 0).updatePosition();
			}

			if (animationProgress == 1.0)
			{
				originalYear -= Mathf.Clamp(targetYear-originalYear, 1, -1);
				animationProgress = 0.0f;

				//change year text values and remove offset
				for (int i = 0; i < TOTAL_YEARS_TO_DISPLAY; i++)
				{
					timelineTexts[i].modify((originalYear-ConstantHelpers.TIMELINE_YEARS_EITHER_SIDE+i).ToString())
								    .offset(0, 0)
								    .updatePosition();
				}
			}
			subscriptionService.Notify(TimelineNotificationEvent.ANIMATION_STEP);
		}
		return this;
	}

	public float getCurrentIntermediateYear(){
		return originalYear + Mathf.Clamp(targetYear-originalYear, -1.0f, 1.0f)*animationProgress;
	}

	/// <summary>
	/// Is this year within the bounds of our timeline min and max?
	/// </summary>
	/// <returns><c>true</c> if the year is within the bounds; otherwise, <c>false</c>.</returns>
	/// <param name="year">Year to check</param>
	private bool IsYearWithinBounds(int year) 
	{
		return minYear <= year && year <= maxYear + maxDataProjection;
	}

	/// <summary>
	/// Triggered when the + key is pushed down. Changes the
	/// year that the timeline is currently representing up a year.
	/// </summary>
	/// <remarks>Triggers YEAR_CHANGE_UP event.</remarks>
	private void HandleIncrementPressed()
	{
		if (IsYearWithinBounds(currentYear + 1)) 
		{
			currentYear++;
			subscriptionService.Notify(TimelineNotificationEvent.YEAR_CHANGE_UP);
		}
		incrementTime = 0;
	}

	/// <summary>
	/// Triggered when the - key is pushed down. Changes the
	/// year that the timeline is currently representing down a year.
	/// </summary>
	/// <remarks>Triggers YEAR_CHANGE_DOWN event.</remarks>
	private void HandleDecrementPressed()
	{
		if (IsYearWithinBounds(currentYear - 1))
		{
			currentYear--;
			subscriptionService.Notify(TimelineNotificationEvent.YEAR_CHANGE_DOWN);
		}
		decrementTime = 0;
	}

	/// <summary>
	/// Gets the maximum year of the dataset.
	/// </summary>
	public int getMaxYear()
	{
		return maxYear;
	}
	
	// GETTERS & SETTERS
	
	/// <summary>
	/// Returns the SubcriptionService owned by this Timeline.
	/// </summary>
	public SubscriptionService<Timeline, TimelineNotificationEvent> Subscription()
	{
		return subscriptionService;
	}

	/// <summary>
	/// Returns the current year on the timeline.
	/// </summary>
	public float CurrentYear()
	{
		return currentYear;
	}
}

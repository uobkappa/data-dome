using UnityEngine;
using System.Collections;
using System.Linq;
using SQLiteInterface;
using System;
public class Projection 
{
	private float yIntercept;
	private float gradient;

	public Projection(float[] x, float[] y)
	{
		if (y.Length != x.Length) throw new System.ArgumentException("Lengths of input arrays must match");

		int dataSize = x.Length;
		float xMean = x.Sum()/dataSize;
		float yMean = y.Sum()/dataSize;

		gradient = CalculateGradient(x, y, dataSize, xMean, yMean);
		yIntercept = YIntercept (gradient, xMean, yMean);
	}

	public Projection(){
		yIntercept = 0;
		gradient = 0;
	}

	/// <summary>
	/// Interpolates the y value corresponding to the given x value
	/// </summary>
	/// <returns>Interpolated y</returns>
	/// <param name="x">The corresponding x coordinate.</param>
	public float InterpolateYFromX(float x)
	{
		return (gradient * x) + yIntercept;
	}

	/// <summary>
	/// Interpolates the x value corresponding to the given y value
	/// </summary>
	/// <returns>Interpolated y</returns>
	/// <param name="y">The corresponding y coordinate.</param>
	public float InterpolateXFromY(float y)
	{
		return (y - yIntercept) / gradient;
	}

	/// <summary>
	/// Calculates the gradient of the interpolation line.
	/// </summary>
	/// <returns>The gradient of the line.</returns>
	private float CalculateGradient(float[] x, float[] y, int size, float xMean, float yMean)
	{
		float sigmaXY = 0;
		float sigmaXX = 0;
		float sigmaYY = 0;

		for(int i = 0; i < size; i++)
		{
			sigmaXY += x[i]*y[i];
			sigmaXX += x[i]*x[i];
			sigmaYY += y[i]*y[i];
		}

		return (sigmaXY / Mathf.Sqrt(sigmaXX * sigmaYY)) * (StandardDeviation(y, size, yMean)/StandardDeviation(x, size, xMean));
	}

	/// <summary>
	/// Calculates the Y intercept of the interpolation line.
	/// </summary>
	/// <returns>The Y intercept.</returns>
	private float YIntercept(float m, float xMean, float yMean)
	{
		return yMean - (m * xMean);
	}

	/// <summary>
	/// Calculates the standard deviation of the data given.
	/// </summary>
	/// <returns>The standard deviation.</returns>
	private float StandardDeviation(float[] data, int size, float mean)
	{
		float variance = (Array.ConvertAll (data, item => item * item).Sum () / size) - (mean * mean);
		return Mathf.Sqrt(variance);
	}

	//DEFINE GETTERS
	public float GetYIntercept(){return yIntercept;}
	public float GetGradient(){return gradient;}
}
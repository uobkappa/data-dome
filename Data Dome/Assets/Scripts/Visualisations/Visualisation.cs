using UnityEngine;
using System.Collections;
using SQLiteInterface;
using System;

public abstract class Visualisation : MonoBehaviour
{
	// UNITY MEMBER VARIABLES
	public float year;
	public string wardname;
	public Vector3 center;

	// PROTECTED MEMBER VARIABLES
	protected float bristolMin, bristolMax;
	protected double height_offset = 0.0f;
	protected County text;

	//PRIVATE MEMBER VARIABLES
	private double total_time = 0;
	private float random_mod = 1;

	/// <summary>
	/// Initialise the visualisation, by the time this method is called, the minQuery and maxQuery fields should be set
	/// to the desired queries from the subclass. Subscribes to the timeline and sets the ward name.
	/// </summary>
	public virtual void Start() 
	{
		wardname = gameObject.GetComponent<Ward>().wardName;
		Timeline timeline = GameObject.Find("Timeline").gameObject.GetComponent<Timeline>();
		timeline.Subscription().Subscribe(OnYearChange, TimelineNotificationEvent.YEAR_CHANGE_UP);
		timeline.Subscription().Subscribe(OnYearChange, TimelineNotificationEvent.YEAR_CHANGE_DOWN);
		timeline.Subscription().Subscribe(OnTimelineAnimationStep, TimelineNotificationEvent.ANIMATION_STEP);
		timeline.Subscription().Subscribe(StepAnimation, TimelineNotificationEvent.UPDATE);
		OnYearChange(timeline);
		bristolMin = GetMinValue();
		bristolMax = GetMaxValue();
		random_mod = UnityEngine.Random.value;
		StaticWard.WardNames.addVisualisation(this);
	}

	void Update(){
		text.step ();
	}

	/// <summary>
	/// Handles any year change from the timeline provided.
	/// Performs the data processing and changes to visualisation
	/// </summary>
	/// <param name="timeline">notifier of the event.</param>
	public void OnYearChange(Timeline timeline)
	{
		this.year = timeline.CurrentYear();
		UpdateVisualisationState(year, timeline.getMaxYear());
	}

	public void OnTimelineAnimationStep(Timeline timeline){
		float yearf = timeline.getCurrentIntermediateYear ();
		UpdateVisualisationState (yearf, timeline.getMaxYear());
	}

	public void StepAnimation(Timeline timeline){
		total_time += Time.deltaTime;
		height_offset = 0;//Math.Sin( (total_time+random_mod) * 5 ) / 100;
		UpdateVisualisation();
	}

	// ABSTRACT METHODS

	/// <summary>
	/// Perform update due to change in year, specific to the Visualisation.
	/// </summary>
	public abstract void UpdateVisualisationState(float year, int maxYear);

	protected abstract float GetMinValue();
	protected abstract float GetMaxValue();
	public abstract void UpdateVisualisation();
}
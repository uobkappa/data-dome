using UnityEngine;
using System;

public abstract class Scale : MonoBehaviour
{
	public float percentage;
	internal float maximum;
	internal float minimum;

	public void Start() 
	{
		minimum = 0.0f;
		maximum = 100.0f;
		Transform();
	}

	public void Update()
	{
		// Perform animation transition here?
	}

	public abstract void Transform();
}


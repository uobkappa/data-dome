using UnityEngine;
using System.Collections;
using SQLiteInterface;
using System;

public class WardEnergyVisualisation : Visualisation
{
	public float currentEnergy = 0;
	public float targetEnergy;
	private Projection projection;

	// Queries
	private const string QUERY_ENERGY_FOR_YEAR = "SELECT EnergyConsumption FROM EnergyConsumptionByWard_AnnualTotal_Normalised WHERE WardName='{0}' AND DateYear={1}";
	private const string QUERY_MAX_CONSUMPTION = "SELECT max(EnergyConsumption) FROM EnergyConsumptionByWard_AnnualTotal_Normalised WHERE WardName != 'Bristol'";
	private const string QUERY_MIN_CONSUMPTION = "SELECT min(EnergyConsumption) FROM EnergyConsumptionByWard_AnnualTotal_Normalised";
	private const string QUERY_ENERGY_FOR_ALL_YEARS = "SELECT DateYear, EnergyConsumption FROM EnergyConsumptionByWard_AnnualTotal_Normalised WHERE WardName='{0}'";

	private SQLiteQuery minQuery, maxQuery, allQuery;

	// Memoization
	private float lowerYear = 0;
	private float upperYear = 0;
	private float lowerYearEnergy = 0;
	private float upperYearEnergy = 0;

	// Use this for initialization
	public override void Start()
	{

		base.Start();
		Debug.Log (transform.GetChild (0).GetComponent<Transform>());
		center = transform.GetChild(0).GetComponent<Renderer>().bounds.center;

		//Initialize projection
		allQuery = SQLiteDatabase.Get ("data").Query(String.Format(QUERY_ENERGY_FOR_ALL_YEARS, wardname));
		allQuery.Execute();

		float[] xs = Array.ConvertAll(allQuery.TransposeResultsArray()[0].ToArray(), item => (float) (Int64) item);
		float[] ys = Array.ConvertAll(allQuery.TransposeResultsArray()[1].ToArray(), item => (float) item);
		projection = new Projection(xs, ys);

		text = ScriptableObject.CreateInstance("County") as County;
		text.init (wardname, center/*new Vector3(0.0f,0.0f,0.0f)*/, /*gameObject*/this.transform.gameObject);
		//text.text.transform.parent = this;
		UpdateVisualisationState(year, Mathf.RoundToInt(year));
	}

	public override void UpdateVisualisationState(float currentYear, int maxYear)
	{
		if (currentYear > maxYear) 
		{
			//USE PROJECTED DATA
			currentEnergy = projection.InterpolateYFromX(currentYear);
			//Debug.Log(currentEnergy);
		} 
		else 
		{
			if (Mathf.FloorToInt(currentYear) != lowerYear) 
			{
				lowerYear = Mathf.FloorToInt(currentYear);
				SQLiteQuery query = SQLiteDatabase.Get("data").Query(String.Format(QUERY_ENERGY_FOR_YEAR, wardname, lowerYear));
				query.Execute();
				lowerYearEnergy = (float) query.ResultsArray()[0][0];
			}

			if (Mathf.CeilToInt(currentYear) != upperYear) 
			{
				upperYear = Mathf.CeilToInt(currentYear);
				SQLiteQuery query = SQLiteDatabase.Get("data").Query(String.Format(QUERY_ENERGY_FOR_YEAR, wardname, upperYear));
				query.Execute();
				upperYearEnergy = (float) query.ResultsArray()[0][0];
			}
			// Take the decimal component of currentYear
			float weight = currentYear % 1;
			currentEnergy = (weight * upperYearEnergy) + ((1 - weight) * lowerYearEnergy);
		}
		UpdateVisualisation();
	}

	protected override float GetMinValue()
	{
		minQuery = SQLiteDatabase.Get("data").Query(QUERY_MIN_CONSUMPTION);
		minQuery.Execute();
		return (float) (double) minQuery.ResultsArray()[0][0];
		//return 0;
	}

	protected override float GetMaxValue()
	{
		
		maxQuery = SQLiteDatabase.Get("data").Query(QUERY_MAX_CONSUMPTION);
		maxQuery.Execute();
		return (float) (double) maxQuery.ResultsArray()[0][0];
		//return 10000;
	}
	
	public override void UpdateVisualisation()
	{
		float bristolMax = GetMaxValue();
		float bristolMin = GetMinValue();
		transform.GetChild(0).GetComponent<Renderer>().material.color = Scales.thermalScale(currentEnergy, bristolMin, bristolMax);
		transform.localScale = new Vector3(1.0f, 1.0f, Mathf.Clamp(0.1f+Scales.basicNumerical(currentEnergy, bristolMin, bristolMax)*0.9f, 0.1f, 2.5f) + (float) height_offset );
    }

}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimelineText : ScriptableObject 
{
	private GameObject text;
	private Text textComponent;
	private float x, y, offsetX, offsetY;
	private string date;

	public TimelineText() {}

	public void init(string date, int x, int y)
	{
		init(date, (float) x, (float) y);
	}

	public void init(string date, float x, float y)
	{
		this.x = x + ConstantHelpers.TIMELINE_OFFSET_X;
		this.y = y + ConstantHelpers.TIMELINE_OFFSET_Y;
		this.date = date;
		addToScene();
	}

	/// <summary>
	/// Adds the timeline text to into the main scene.
	/// </summary>
	private void addToScene()
	{
		Canvas canvasComponent = GameObject.Find("Timeline").GetComponent<Canvas>();
		text = Instantiate(Resources.Load("TimelineDate", typeof(GameObject))) as GameObject;
		text.transform.position = new Vector3(x, y, 0);
		text.transform.SetParent(canvasComponent.gameObject.transform, false);
		textComponent = text.transform.GetComponent<Text> ();
		textComponent.text = date;
	}

	/// <summary>
	/// Change the text displayed to match the new date without modfiying position.
	/// </summary>
	/// <param name="date">New date to change to.</param>
	public TimelineText modify(string date)
	{
		return modify(date, x, y);
	}

	/// <summary>
	/// Change the text displayed to match the new date, also adjusting the (x, y) coords.
	/// </summary>
	/// <param name="date">New date to change to.</param>
	/// <param name="x">New x position.</param>
	/// <param name="y">New y position.</param>
	public TimelineText modify(string date, float x, float y)
	{
		this.date = date;
		textComponent.text = date;
		this.x = x;
		this.y = y;
		return this;
	}

	/// <summary>
	/// Sets the offset for the timeline text.
	/// </summary>
	/// <param name="offsetX">New offset in x.</param>
	/// <param name="offsetY">New offset in y.</param>
	public TimelineText offset(float offsetX, float offsetY)
	{
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		return this;
	}

	/// <summary>
	/// Updates the position of the timeline text to be equal to the current (x, y) plus the offset.
	/// </summary>
	public TimelineText updatePosition()
	{
		text.transform.position = new Vector3(x + offsetX, y + offsetY, 0);
		return this;
	}
}

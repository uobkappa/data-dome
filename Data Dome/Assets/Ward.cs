﻿using UnityEngine;
using System.Collections;

public class Ward : MonoBehaviour 
{
    public string wardName;
	private County text;
	public bool wardEnergy = true;
	public bool solar = false;

	void Start () 
	{
		if (wardEnergy) {
			gameObject.AddComponent<WardEnergyVisualisation> ();
		} 
		else if (solar) {
			gameObject.AddComponent<WardSolarEnergyVisualisation> ();
		}
		wardName = transform.name;
	}
}

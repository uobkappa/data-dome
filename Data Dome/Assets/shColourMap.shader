﻿Shader "Custom/shColourMap" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	_MainTex2("Albedo (RGB)", 2D) = "white" {}
	_merge("Merge", Range(0,1)) = 0.0
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;
	sampler2D _MainTex2;
	float     _merge;

	struct Input {
		float2 uv_MainTex;
		float3 worldPos;
	};

	half _Glossiness;
	half _Metallic;
	fixed4 _Color;

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 cA = tex2D(_MainTex, 1.0 - ((IN.worldPos.xz) / float2(780,850))) * _Color;
		fixed4 cB = tex2D(_MainTex2, 1.0 - ((IN.worldPos.xz) / float2(780, 850))) * _Color;
		fixed4 c = cA;
		c.rgb = lerp(cA.rgb, cB.rgb, _merge);
		o.Albedo = c.rgb*clamp((IN.worldPos.y / 50.0)*1.0 + 0.30, 0.15, 1.0);
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		o.Alpha = c.a;
	}
	ENDCG
	}
		FallBack "Diffuse"
}
